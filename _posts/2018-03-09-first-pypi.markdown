---
layout: post
title:  "My first Pypi package for pip"
date:   2018-03-09 09:55:45
categories: Python Pypi Package
---
Putting a new package on Pypi was really easy thanks to the online and interactive help during submission process (Error diagnostics + associated url help pointers).


After a registration on Pypi and Pypi-test portal, email confirmation, I just created my .pypirc and run next commands :
{% highlight shell %}
python setup.py sdist upload -r pypitest
python setup.py sdist upload -r pypi
{% endhighlight %}


[Peterdowns tutorial](http://peterdowns.com/posts/first-time-with-pypi.html) helped me to get general schema then moving to [official doc](https://pypi.org/help/) gave me up-to-date procedures that I will try next time ;) .
 
This is always nice and source of motivation to put resources in such a collaborative environment.

This was for a new project called [A2P2](https://pypi.org/project/a2p2/)

