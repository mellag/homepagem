---
layout: post
title:  "Ansible Basic"
date:   2018-01-22 08:55:43
categories: ansible script
---

We find a lot of resources on internet about Ansible to handle server deployement and configuration.

Here is a command line just to highlight that any command can be run on a group of machine from an existing inventory:
{% highlight shell %}
ansible -i <inventory> <servergroup> -a "whoami"
{% endhighlight %}

If you wan to run some more complex command, the shell module is adviced: 
{% highlight shell %}
ansible -i jmmc-srv-infra/inventories/prod all -m shell -a "mount | grep nfs"
{% endhighlight %}



