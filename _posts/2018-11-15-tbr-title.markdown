---
layout: post
title:  "Continuous Automation on Gitlab"
date:   2018-11-15 14:31:30
categories: DevOps
---
Here come a small post to mention that getting access to a Gitlab forge provides more than just git/code revision tool.

Indeed Gitlab offer Continuous Integration with huge collection of features.

I now succeeded my attend to generate myblog content just after my `git push` commands using Gitlab/Pages

A special thank to Remi for his training practical session and big thanks to the maintainers of our Gitlab instance hosted by [Gricad/UGA](https://gricad.univ-grenoble-alpes.fr/thematiques/forge) people list TBD.

As Remi repeats [Read the Fantastic User Manual](https://docs.gitlab.com/ce/user/project/pages/)
