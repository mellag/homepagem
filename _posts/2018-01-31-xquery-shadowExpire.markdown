---
layout: post
title:  "Compute date from shadowExpire in xquery"
date:   2018-01-31 20:16:48
categories: xquery unix
---

You can get the shadowExpire value from the 8th field of next command result: 
{% highlight shell %}
$ getent shadow yourlgin
yourlgin:*::::::17590:0
$ date --date="@$((17590*86400))"
mercredi 28 février 2018, 01:00:00 (UTC+0100)
{% endhighlight %}

or convert given value using this simple query:
{% highlight xquery %}
datetime:timestamp-to-datetime( 17590*86400*1000 )
{% endhighlight %}


In the other side:
{% highlight xquery %}
xs:integer(datetime:timestamp(current-dateTime()) div 86400000) 
{% endhighlight %}
