---
layout: post
title:  "Welcome to my Homepage!"
date:   2018-01-08 16:56:20
categories: general
---
Happy new year 2018 !!

Moving from IPAG to OSUG-DC late 2017, here comes my new personal homepage using Jekyll.

I still will check out the [Jekyll docs][jekyll] for more info on how to get the most out of Jekyll. File all bugs/feature requests at [Jekyll's GitHub repo][jekyll-gh].

[jekyll-gh]: https://github.com/jekyll/jekyll
[jekyll]:    http://jekyllrb.com
