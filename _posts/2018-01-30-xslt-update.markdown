---
layout: post
title:  "simple XSLT update"
date:   2018-01-30 09:37:30
categories: xml xslt
---

Please find below an XSLT stylesheet which demonstrates how to upgrade a given xml to a similar one using some priority attributes.
It leaves comments attributes and most element not covered by higher priority templates.


{% highlight xslt %}
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
    <xsl:output omit-xml-declaration="yes" indent="no"/>
    <!--                                                       -->
    <!-- just pretty format WOS records to simple xml elements -->
    <!--                                                       -->
    <xsl:template match="/">
            <xsl:apply-templates />
    </xsl:template>

    <!-- Copy elements as original -->
    <xsl:template match="*" priority="1">
        <xsl:element name="{name()}">
            <xsl:apply-templates select="./@*"/>
            <xsl:apply-templates />
        </xsl:element>
    </xsl:template>

    <!-- Copy comment text and attributes as original -->
    <xsl:template match="comment()|text()|@*">
        <xsl:copy-of select="."/>
    </xsl:template>

   <!-- this template match owner before general * ones -->
   <!-- so we can then adjust it manually -->
   <xsl:template match="//records/*[label]" priority="2">
            <xsl:apply-templates select="./value"/>
   </xsl:template>

   <xsl:template match="//records/*[label]/value" priority="2">
     <xsl:element name="{translate(../label, '.','_') }">
            <xsl:apply-templates/>
        </xsl:element>
   </xsl:template>
</xsl:stylesheet>
{% endhighlight %}
