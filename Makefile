all: build publish

new:
	echo "---\nlayout: post\ntitle:  \"TBR_title\"\ndate:   $$(date '+%Y-%m-%d %X')\ncategories: TBD\n---\n\n https://jekyllrb.com/docs/posts/" > newpost
	mv -v newpost _posts/$$(date "+%Y-%m-%d-tbr-title.markdown")

build:
	sed -i "s@JEKYLL_BASEURL@/~mellag/@g" _config.yml
	sed -i "s@JEKYLL_URL@https://apps.jmmc.fr/@g" _config.yml
	jekyll build

help:
	jekyll doc

publish:
	rsync -a _site/ jmmc.Fr:public_html/

lview:
	firefox _site/index.html

view:
	firefox http://apps.jmmc.fr/~mellag/

commit:
	git add _posts/; git commit -a -m "new post or update"; git push

clean:
	rm -rf _site/
	rm -rf _posts/$$(date "+%Y-%m-%d-tbr-title.markdown")
